# SUMÁRIO

- [TCP](#tcp)
    - [Exemplo](#exemplo)
- [UDP](#udp)
    - [Exemplo](#exemplo)
- [HTTP](#http)
    - [Como HTTP funciona](#como-http-funciona)
    - [Como HTTPS funciona](#como-https-funciona)
    - [HTTP 1.0](#http-10)
    - [HTTP 1.1](#http-11)
    - [HTTP 2.0](#http-20)
    - [HTTP 3.0](#http-30)
- [REFERÊNCIA](#referência)


# TCP

O protocolo TCP garante a entrega dos pacotes de dados, de forma ordenada, porém ele necessita de muitas informações por pacote para garantir que tudo ocorra como esperado. Sendo assim ele é um método mais lento.

Outra característica é que o servidor é stateful, ou seja, ele guarda informações sobre a conexão (pois ele precisa garantir que tudo ocorra bem, logo ele precisa disso para monitorar), logo se o servidor cair, ou for reiniciado, o client será desconectado imediatamente.

O fato de cair o client, se deve ao fato de que os dois pontos estão conectados, logo o servidor sabe quem é o client (veja o exemplo), logo ele também pode barrar requisições de clients indesejados/desconhecidos o que melhora a segurança.

## Exemplo

No exemplo para o protocolo, criei um servidor com python, que recebe dados e o retorna. Sendo que o cliente é telnet que já vem no sistema (no meu caso, linux).

<a href='./tcp'>Código...</a>

<img src="./images/figura_01.gif"/>


# UDP

Ao contrário do TCP, o UDP não garante a entrega, pois não há monitoramento dos pacotes. Isso auxilia no fato de que os pacotes são menores, e como não precisa de confirmação por parte do cliente, o processo de envio de pacotes é muito mais rápido. Porém corre o risco de perder dados. Então tem que analisar em quais casos isso é ideal (stream de videos e jogos por exemplo).

Como o UDP não estabelece conexão entre client e server (como no TCP), qualquer client pode requisitar informação do servidor, e isso pode ser uma falha de segurança.

Como não existe conexão entre client e servidor, não existe nenhuma informação do client no servidor, logo ele é stateless, o que permite reiniciar o servidor sem o client sofrer qualquer interferência (além de não conseguir requisitar nada).

## Exemplo

Criei um server em python, que recebe uma mensagem, e a retorna para o client, e o client está em shell utilizando o nc. Veja no GIF abaixo, que assim que envio a requisição, passado o time out (-wx sendo x os segundos), o client encerra, tanto que o endereço dele sempre muda (veja os prints do server).

> OBS.: Quando não coloco o timeout veja que o tempo de espera foi meio longo, e o timeout não pode ser decimal

<a href='./udp'>Código...</a>

<img src="./images/figura_02.gif"/>


# HTTP

HTTP serve para transferir imagens, textos, arquivos em geral em uma estrutura de client/server. Sendo que basicamente toda a internet é baseada nisso.

- Request HTTP é composta de:
    - URL: Endereço para o qual a request será feita
    - Method Type:
        - GET: Buscar informação
        - POST: Enviar informação para ser inserida
        - PUT: Enviar informação para atualizá-la
        - DELETE: Enviar informação para deletar algo
    - Headers: Contém informações como por exemplo Tokens, Credenciais de autorizações, tipos de dados...
    - Body: Pode ser um json, uma imagem, em suma, o conteúdo a ser salvo. Sendo que o método GET não possui Body.

- Response HTTP é composta de:
    - Status Code: Refere-se ao status da sua request, se ela ocorreu com sucesso (200) se não encontrou o que procurava (404).
    - Headers: Contém informações
    - Body: Dados que retonam nas buscas (GET por exemplo)

## Como HTTP funciona

Ele foi implementado em cima do TCP, sendo que todos esses componentes/informações de uma request/response são transportados entre client server via TCP. Logo os passos para o HTTP são:


- Abrir uma conexão client/server (porta 80)
- O client envia informações requisitando algo
- O servidor responde a requisição com informações (html, json, imagens)
- E encerra a conexão


## Como HTTPS funciona

Em essência o HTTPS e o HTTP funcionam da exata mesma maneira. Porém quando uma conexão é aberta entre client/server ela é feita na porta 443, e existe uma coisa chamada `Handshake`. Sendo que ambas as partes possuem uma chave idêntica, e quando o client envia uma informação, essa informação é criptografada com base nessa chave, e quando ela chega no servidor ele descriptografa utilizando o par de chave que ele possui, interpreta, faz o que tem que fazer e devolve informação criptografada também.


## HTTP 1.0

O problema aqui era que a conexão TCP era encerrada a cada request. Isso foi feito pois a conexão TCP consome uma quantidade de memória RAM que para época era considerável. Só que isso deixa cada request mais demorada, pois ficamos abrindo e fechando conexões toda hora, e o TCP não é muito rápido.

## HTTP 1.1

O cliente fica constantemente informando que ainda precisa da conexão. Logo o servidor não encerra a conexão até que ela não seja mais necessária. Isso faz com que não precisamos mais ficar abrindo e fechando conexões TCP. 

E as informações começaram a serem enviadas por meio de stream, e não por buffer, ou seja, eles eram enviadas conforme o servidor as processáva, evitando que ele ficasse acumulando informação para enviá-las tudo de uma vez. Isso gerava aquele efeito de imagem carregando em partes.

## HTTP 2.0

Foram adicionados:

- Multiplexing: 
    - Unir varias requests em uma única conexão TCP.
- Compressão: 
    - A informação sendo comprimida com protocolos de compressão open source ficaram menores e melhoraram a velocidade.
- Server Push: 
    - Conexões bidirecionais, sendo assim o servidor não precisa receber a request para enviar algo ao client (notificações por exemplo).
- Secure By Default:
    - Sempre HTTPS
- Protocol Negotiation During TLS (NPN/ALPN):
    - O servidor pode escolher qual dentre os protocolos disponíveis na request ele quer trabalhar (para manter o legado 1.0/1.1)


## HTTP 3.0

É o http 2.0, com o TCP pelo QUIC, que é o UDP com controle de perda de informação.

# REFERÊNCIA

Veja os vídeos 
- [TCP vs UDP Crash Course - Hussein Nasser](https://www.youtube.com/watch?v=qqRYkcta6IE&list=PLQnljOFTspQUNnO4p00ua_C5mKTfldiYT&index=4)

- [Hyper Text Transfer Protocol Crash Course - HTTP 1.0, 1.1, HTTP/2, HTTP/3 - Hussein Nasser](https://www.youtube.com/watch?v=0OrmKCB0UrQ&list=PLQnljOFTspQUNnO4p00ua_C5mKTfldiYT&index=6)