import socket

HOST = '127.0.0.1'
PORT = 8081

with socket.socket(socket.AF_INET, socket.SOCK_DGRAM) as s:
    s.bind((HOST, PORT))
    while True:
        data = s.recvfrom(1024)
        msg, addr = data[0:2]

        print(f'Client: {addr} - Msg: {msg.decode()}')
        s.sendto(msg, addr)
